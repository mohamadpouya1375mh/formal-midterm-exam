# Formal Midterm exam

## Problem:
Solve a sudoku page automatically.

  

## Problem Statement:
Solving a Sudoku page automatically. Sudoku is a logic game that consists of a 9x9 board, where parts of it are filled with numbers from 1 to 9, even some of these cells remain empty. The player's goal is to fill the empty squares so that each number appears only once in each row, column, and 9x9 block.

In this project, a computer program is required to be able to solve a variety of Sudoku pages. Specifically, the input of the program is a sudoku board with a number of empty cells, and the output is a board filled with integers that satisfy the game conditions.

To solve this problem, the program must use appropriate algorithms that are able to fill the empty boxes with integers, following the rules of the Sudoku game. These algorithms may include a combination of different methods such as recursive search, combinatorial algorithms or specific algorithms for Sudoku.

## Requirement:


The program should be able to receive a sudoku page with a number of empty cells in the form of a tuple from the user.

The program should be able to automatically solve the sudoku puzzle if it is solvable.

The program should give us an output, either a solved sudoku table (in the form of a matrix) or a message if it does not have the ability to solve the problem.

  

The limitations of the problem are as follows:

1. Dimensions of the board: Sudoku board should be 9 x 9.
2. Input Limits: The input field must contain integers from 1 to 9 or blank (zero or dot).
3. Board Solving Limits: The sudoku board must be solvable and each number must be used at most once in each row, column, and 9x9 block.
  

assumptions:
1. Validity of input data: It is assumed that the input sudoku page is valid and solvable.
2. Algorithm type: It is assumed that appropriate and optimal algorithms are used to solve Sudoku.

  
Non-functional requirements:

1. Performance: The program should solve the Sudoku board in a reasonable amount of time and with acceptable efficiency.
2. Usability: The user interface should be easy to use and understand, which is omitted due to time constraints.

  
  
  

## Specification:


Technical Specifications:

1. Programming language:
Python is chosen because of its simplicity, flexibility and ability to use appropriate libraries for data processing and solving logic problems.

2. Development environment: Google Colab
3. Python Version: To maintain compatibility and support, Python version 3.x will be used.
4. User interface: The user interface is in text format and the input and output are displayed in text format.

## Files

The main code stored in index file